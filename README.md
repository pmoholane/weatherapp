# WeatherApp
 - This is a Symfony Project created for RSAWEB Assessment
 - Check if your computer meets all requirements [symfony check:requirements]
 - To run the project, you need to run [composer install] or [php composer.phar install] within the directory
 - Run the project by running this command [symfony server:start]
 - The default url should be [http://localhost:8000/]
 - I had added the screenshots in the project folder [project_folder/screenshots]

 # NiceToHave
 - An API that would get data from the South African Weather Service so that we have a feed of any alerts they issue out. For now I did a mockup on the page.
 - It would have been nice to add a 5 day weather focus on the page below, unfortunately this is a paid service however I would have liked to implement this [https://openweathermap.org/api/forecast30]
 - Also I would have liked to be able to get the client IP dynamically, for now I have set it to a static IP which allows us to get the default weather for that IPs location.