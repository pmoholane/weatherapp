<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType; 
use Symfony\Component\Form\Extension\Core\Type\FormType; 

class WeatherController extends AbstractController
{
    public function index(Request $request): Response
    {
        $iplookup = $this->ipLookup(); // We do an IP lookup when the page is loaded by default
        
        // From the IP we get the logitude and the latitude
        // We use them to search for the current weather details
        $iplongitude = $iplookup['longitude'];
        $iplatitude = $iplookup['latitude'];
        $currentWeather = $this->currentWeather($iplongitude, $iplatitude);
        $error_message = '';

        //We create our form to search other locations
        $form = $this->createFormBuilder()
            ->add('name', TextType::class, 
                array(
                    'label' => false,
                    'attr' => array(
                    'placeholder' => 'Enter Location',
                    'class' => 'form-control'
                )))
            ->getForm();

        $form->handleRequest($request);

        //If form is submitted, we return Weather patterns of the new location
        if ($form->isSubmitted()) {
            $data = $form->getData();

            // Passing the form data to Geolocation so that we can get the coordinates
            $aGeolocation = $this->forwardGeoLocation($data['name']);
            
            if(!empty($aGeolocation['data'])){ // We need to check if the search location does exists or we return an error message
                
                // The coordinates were found and we use the longitude and the latitude to get current weather of that location
                $longitude = $aGeolocation['data'][0]['longitude'];
                $latitude = $aGeolocation['data'][0]['latitude'];
                $newWeatherLocation = $this->currentWeather($longitude, $latitude);

                // We pass our variables to the template so that we render the data
                return $this->render('view/weather.html.twig', [
                    'page_title' => 'Weather App',
                    'ip_lookup' => $iplookup,
                    'geolocation' => $aGeolocation,
                    'currentWeather' => $currentWeather,
                    'newWeatherLocation' => $newWeatherLocation,
                    'form' => $form->createView(),
                    'error_message' => $error_message
                ]);
            }
            else{ // Location was not found so we return a gracefull error message
                $error_message = $data['name']." could not be found. Please use another term.";
            }  
        }

        // Default data when we load the page or when the searched location was not found.
        return $this->render('view/weather.html.twig', [
            'page_title' => 'Weather App',
            'ip_lookup' => $iplookup,
            'currentWeather' => $currentWeather,
            'form' => $form->createView(),
            'error_message' => $error_message
        ]);
    }


    // API call to get the Geolocation longitude and the latitude we need to get the weather conditions
    public function forwardGeoLocation($location = null){
        $access_key = '26bc0f7a88581cc3a0127ca1e9c65dda';
        
        if($location == null){
            $queryString = http_build_query([
                'access_key' => $access_key,
                'query' => '18 Linda Street',
                'region' => 'South Africa',
                'output' => 'json',
                'limit' => 1,
              ]);
        }
        else{
            $queryString = http_build_query([
                'access_key' => $access_key,
                'query' => $location,
                'region' => 'South Africa',
                'output' => 'json',
                'limit' => 1,
              ]);
        }

        $ch = curl_init(sprintf('%s?%s', 'http://api.positionstack.com/v1/forward', $queryString));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        curl_close($ch);
        return json_decode($json, true);
    }


    /*  Client IPLookup
    **  get current location
    **  return array
    */
    public function ipLookup(){
        // https://ipstack.com/documentation

        // set IP address and API access key
        $ip = '41.144.159.89';
        $access_key = '57c534f60319aafb2281e734c0c38c0d';

        // Initialize CURL:
        $ch = curl_init('http://api.ipstack.com/'.$ip.'?access_key='.$access_key.'');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);

        // Decode JSON response:
        return json_decode($json, true);
    }


    // API call to get the weather conditions either for current location or for the searched location
    public function currentWeather($longitude, $latitude){
        $apiKey = '064b01c7fce574f23620f2ee7486878d';

        // Initialize CURL:
        $ch = curl_init('https://api.openweathermap.org/data/2.5/weather?lat='.$latitude.'&lon='.$longitude.'&appid='.$apiKey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);

        // Decode JSON response:
        return json_decode($json, true);
    }
}