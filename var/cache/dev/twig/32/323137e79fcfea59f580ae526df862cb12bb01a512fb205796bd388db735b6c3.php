<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* view/weather.html.twig */
class __TwigTemplate_774abc1f6ce7449ed6203751e03bfcf447d1954a5c07ae64ababa67f8dfa5dd4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "view/weather.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <title>Welcome to Symfony!</title>
        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
        <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    </head>
    <body>

        <div class=\"header py-2 px-5\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6\">
                    <img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo.jpeg"), "html", null, true);
        echo "\" class=\"logo\">
                </div>

                <div class=\"col-12 col-md-6\">
                    <div class=\"current_location pt-4 align-items-right\">
                    <strong><i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Current Location:</strong> <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 19, $this->source); })()), "location", [], "any", false, false, false, 19), "country_flag", [], "any", false, false, false, 19), "html", null, true);
        echo "\" width=\"15\" style=\"margin-top: -4px;\"> ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 19, $this->source); })()), "city", [], "any", false, false, false, 19), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 19, $this->source); })()), "region_name", [], "any", false, false, false, 19), "html", null, true);
        echo "  ";
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 19, $this->source); })()), "main", [], "any", false, false, false, 19), "temp", [], "any", false, false, false, 19) - 273.15), "html", null, true);
        echo "° C <img src=\"http://openweathermap.org/img/w/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 19, $this->source); })()), "weather", [], "any", false, false, false, 19), 0, [], "any", false, false, false, 19), "icon", [], "any", false, false, false, 19), "html", null, true);
        echo ".png\">
                    </div>
                </div>

            </div>

        </div>

        <div class=\"link-section\">
            <div class=\"container\">
                
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 pt-3\">
                        <ul class=\"links\">
                            <li><a href=\"\">TODAY</a></li>
                            <li><a href=\"\">HOURLY</a></li>
                            <li><a href=\"\">TOMOROW</a></li>
                            <li><a href=\"\">7 DAYS FORCAST</a></li>
                        </ul>
                    </div>

                    <div class=\"col-12 col-md-6\">
                        <form method=\"POST\" action=\"\" class=\"pt-4 align-items-right\">
                            <div class=\"form-row\">
                                
                                <div class=\"col-auto\">
                                    <label class=\"sr-only\" for=\"inlineFormInputGroup\">Location</label>
                                    <div class=\"input-group mb-2\">
                                        <div class=\"input-group-prepend\">
                                        <div class=\"input-group-text\"><i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i></div>
                                        </div>
                                        <input type=\"text\" class=\"form-control\" id=\"inlineFormInputGroup\" placeholder=\"Location\">
                                    </div>
                                </div>

                                <div class=\"col-auto\">
                                    <button type=\"submit\" class=\"btn btn-primary mb-2\">Submit</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div class=\"py-5\">
            <div class=\"container\">
                <h2>Current Weather: ";
        // line 68
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 68, $this->source); })()), "weather", [], "any", false, false, false, 68), 0, [], "any", false, false, false, 68), "description", [], "any", false, false, false, 68), "html", null, true);
        echo " <img src=\"http://openweathermap.org/img/w/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 68, $this->source); })()), "weather", [], "any", false, false, false, 68), 0, [], "any", false, false, false, 68), "icon", [], "any", false, false, false, 68), "html", null, true);
        echo ".png\"></h2>

                <div class=\"row\">
                    <div class=\"col-12 col-md-6\">
                        <div class=\"weather-forcast-container p-5\">

                            <div class=\"conditions mb-4\">
                                <i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Location: <br/> 
                                ";
        // line 76
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 76, $this->source); })()), "city", [], "any", false, false, false, 76), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 76, $this->source); })()), "region_name", [], "any", false, false, false, 76), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 76, $this->source); })()), "region_name", [], "any", false, false, false, 76), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 76, $this->source); })()), "country_name", [], "any", false, false, false, 76), "html", null, true);
        echo " <img src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 76, $this->source); })()), "location", [], "any", false, false, false, 76), "country_flag", [], "any", false, false, false, 76), "html", null, true);
        echo "\" width=\"17\">
                            </div>

                            <div class=\"row\">
                                
                                <div class=\"col-6 conditions\">
                                    <div class=\"pb-3\">
                                        <i class=\"fa fa-thermometer-empty\" aria-hidden=\"true\"></i> Temperature: <br/>
                                        <div class=\"temp mt-2\">Avg: ";
        // line 84
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 84, $this->source); })()), "main", [], "any", false, false, false, 84), "temp", [], "any", false, false, false, 84) - 273.15), "html", null, true);
        echo "°C</div>
                                        <div class=\"temp\">Feels Like: ";
        // line 85
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 85, $this->source); })()), "main", [], "any", false, false, false, 85), "feels_like", [], "any", false, false, false, 85) - 273.15), "html", null, true);
        echo "°C</div>
                                        <div class=\"temp\">Min: ";
        // line 86
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 86, $this->source); })()), "main", [], "any", false, false, false, 86), "temp_min", [], "any", false, false, false, 86) - 273.15), "html", null, true);
        echo "°C</div>
                                        <div class=\"temp\">Max: ";
        // line 87
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 87, $this->source); })()), "main", [], "any", false, false, false, 87), "temp_max", [], "any", false, false, false, 87) - 273.15), "html", null, true);
        echo "°C</div>
                                        <div class=\"temp\">Pressure: ";
        // line 88
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 88, $this->source); })()), "main", [], "any", false, false, false, 88), "pressure", [], "any", false, false, false, 88), "html", null, true);
        echo " hPa</div>
                                        <div class=\"temp\">Humidity: ";
        // line 89
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 89, $this->source); })()), "main", [], "any", false, false, false, 89), "humidity", [], "any", false, false, false, 89), "html", null, true);
        echo "% </div>
                                    </div>
                                </div>

                                <div class=\"col-6 conditions\">

                                    <div class=\"pb-3\">
                                        <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Wind Direction:
                                        <span>";
        // line 97
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 97, $this->source); })()), "wind", [], "any", false, false, false, 97), "deg", [], "any", false, false, false, 97), "html", null, true);
        echo "°</span>
                                    </div>

                                    <div class=\"pb-3\">
                                        <i class=\"fa fa-tint\" aria-hidden=\"true\"></i> Precipitation:
                                        <span>";
        // line 102
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 102, $this->source); })()), "main", [], "any", false, false, false, 102), "temp", [], "any", false, false, false, 102) - 273.15), "html", null, true);
        echo "</span>
                                    </div>

                                    <div class=\"pb-3\">
                                        <i class=\"fa fa-flag\" aria-hidden=\"true\"></i> Wind Speed:
                                        <span>";
        // line 107
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 107, $this->source); })()), "wind", [], "any", false, false, false, 107), "speed", [], "any", false, false, false, 107), "html", null, true);
        echo " meter/sec</span>
                                    </div>

                                    <div class=\"pb-3\">
                                        <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Cloudiness:
                                        <span>";
        // line 112
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 112, $this->source); })()), "clouds", [], "any", false, false, false, 112), "all", [], "any", false, false, false, 112), "html", null, true);
        echo "%</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class=\"col-12 col-md-6 sa-weather-service\">
                        <h4>South African Weather Service: Weather Alerts</h4>
                        <div class=\"py-3\">
                            <h6>Mossel Bay - Damaging Winds</h6>
                            <div class=\"time-period mb-2\"><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i> 2023/09/15 00:00 - 2023/09/17 23:59:59</div>
                            <p>
                                Difficulty in navigation at sea for small vessels and personal water crafts (e.g. kayaks). 
                                Localised disruptions at susceptible ports due to quick, successive steep waves. 
                                Small vessels at risk of mooring lines breaking and taking on water and capsizing in locality. 
                                Localised build-up of sand on coastal routs. Localised disruption to beachfront activities.
                            </p>

                        <hr>

                        <div class=\"py-3\">
                            <h6>Lamberts Bay - Damaging Winds</h6>
                            <div class=\"time-period mb-2\"><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i> 2023/09/15 00:00 - 2023/09/17 23:59:59</div>
                            <p>
                                Difficulty in navigation at sea for small vessels and personal water crafts (e.g. kayaks). 
                                Localised disruptions at susceptible ports due to quick, successive steep waves. 
                                Small vessels at risk of mooring lines breaking and taking on water and capsizing in locality. 
                                Localised build-up of sand on coastal routs. Localised disruption to beachfront activities.
                            </p>

                        </div>
                    </div>
                </div

            </div>
        </div>


        <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>
    </body>
</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "view/weather.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 112,  205 => 107,  197 => 102,  189 => 97,  178 => 89,  174 => 88,  170 => 87,  166 => 86,  162 => 85,  158 => 84,  139 => 76,  126 => 68,  66 => 19,  58 => 14,  48 => 7,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <title>Welcome to Symfony!</title>
        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
        <link href=\"{{ asset('css/custom.css') }}\" rel=\"stylesheet\" />
    </head>
    <body>

        <div class=\"header py-2 px-5\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6\">
                    <img src=\"{{ asset('images/logo.jpeg') }}\" class=\"logo\">
                </div>

                <div class=\"col-12 col-md-6\">
                    <div class=\"current_location pt-4 align-items-right\">
                    <strong><i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Current Location:</strong> <img src=\"{{ ip_lookup.location.country_flag }}\" width=\"15\" style=\"margin-top: -4px;\"> {{ ip_lookup.city }}, {{ ip_lookup.region_name }}  {{currentWeather.main.temp - 273.15}}° C <img src=\"http://openweathermap.org/img/w/{{currentWeather.weather.0.icon}}.png\">
                    </div>
                </div>

            </div>

        </div>

        <div class=\"link-section\">
            <div class=\"container\">
                
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 pt-3\">
                        <ul class=\"links\">
                            <li><a href=\"\">TODAY</a></li>
                            <li><a href=\"\">HOURLY</a></li>
                            <li><a href=\"\">TOMOROW</a></li>
                            <li><a href=\"\">7 DAYS FORCAST</a></li>
                        </ul>
                    </div>

                    <div class=\"col-12 col-md-6\">
                        <form method=\"POST\" action=\"\" class=\"pt-4 align-items-right\">
                            <div class=\"form-row\">
                                
                                <div class=\"col-auto\">
                                    <label class=\"sr-only\" for=\"inlineFormInputGroup\">Location</label>
                                    <div class=\"input-group mb-2\">
                                        <div class=\"input-group-prepend\">
                                        <div class=\"input-group-text\"><i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i></div>
                                        </div>
                                        <input type=\"text\" class=\"form-control\" id=\"inlineFormInputGroup\" placeholder=\"Location\">
                                    </div>
                                </div>

                                <div class=\"col-auto\">
                                    <button type=\"submit\" class=\"btn btn-primary mb-2\">Submit</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div class=\"py-5\">
            <div class=\"container\">
                <h2>Current Weather: {{currentWeather.weather.0.description}} <img src=\"http://openweathermap.org/img/w/{{currentWeather.weather.0.icon}}.png\"></h2>

                <div class=\"row\">
                    <div class=\"col-12 col-md-6\">
                        <div class=\"weather-forcast-container p-5\">

                            <div class=\"conditions mb-4\">
                                <i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Location: <br/> 
                                {{ip_lookup.city}}, {{ip_lookup.region_name}}, {{ip_lookup.region_name}}, {{ip_lookup.country_name}} <img src=\"{{ip_lookup.location.country_flag}}\" width=\"17\">
                            </div>

                            <div class=\"row\">
                                
                                <div class=\"col-6 conditions\">
                                    <div class=\"pb-3\">
                                        <i class=\"fa fa-thermometer-empty\" aria-hidden=\"true\"></i> Temperature: <br/>
                                        <div class=\"temp mt-2\">Avg: {{currentWeather.main.temp - 273.15}}°C</div>
                                        <div class=\"temp\">Feels Like: {{currentWeather.main.feels_like - 273.15}}°C</div>
                                        <div class=\"temp\">Min: {{currentWeather.main.temp_min - 273.15}}°C</div>
                                        <div class=\"temp\">Max: {{currentWeather.main.temp_max - 273.15}}°C</div>
                                        <div class=\"temp\">Pressure: {{currentWeather.main.pressure}} hPa</div>
                                        <div class=\"temp\">Humidity: {{currentWeather.main.humidity}}% </div>
                                    </div>
                                </div>

                                <div class=\"col-6 conditions\">

                                    <div class=\"pb-3\">
                                        <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Wind Direction:
                                        <span>{{currentWeather.wind.deg}}°</span>
                                    </div>

                                    <div class=\"pb-3\">
                                        <i class=\"fa fa-tint\" aria-hidden=\"true\"></i> Precipitation:
                                        <span>{{currentWeather.main.temp - 273.15}}</span>
                                    </div>

                                    <div class=\"pb-3\">
                                        <i class=\"fa fa-flag\" aria-hidden=\"true\"></i> Wind Speed:
                                        <span>{{currentWeather.wind.speed}} meter/sec</span>
                                    </div>

                                    <div class=\"pb-3\">
                                        <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Cloudiness:
                                        <span>{{currentWeather.clouds.all}}%</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class=\"col-12 col-md-6 sa-weather-service\">
                        <h4>South African Weather Service: Weather Alerts</h4>
                        <div class=\"py-3\">
                            <h6>Mossel Bay - Damaging Winds</h6>
                            <div class=\"time-period mb-2\"><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i> 2023/09/15 00:00 - 2023/09/17 23:59:59</div>
                            <p>
                                Difficulty in navigation at sea for small vessels and personal water crafts (e.g. kayaks). 
                                Localised disruptions at susceptible ports due to quick, successive steep waves. 
                                Small vessels at risk of mooring lines breaking and taking on water and capsizing in locality. 
                                Localised build-up of sand on coastal routs. Localised disruption to beachfront activities.
                            </p>

                        <hr>

                        <div class=\"py-3\">
                            <h6>Lamberts Bay - Damaging Winds</h6>
                            <div class=\"time-period mb-2\"><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i> 2023/09/15 00:00 - 2023/09/17 23:59:59</div>
                            <p>
                                Difficulty in navigation at sea for small vessels and personal water crafts (e.g. kayaks). 
                                Localised disruptions at susceptible ports due to quick, successive steep waves. 
                                Small vessels at risk of mooring lines breaking and taking on water and capsizing in locality. 
                                Localised build-up of sand on coastal routs. Localised disruption to beachfront activities.
                            </p>

                        </div>
                    </div>
                </div

            </div>
        </div>


        <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>
    </body>
</html>", "view/weather.html.twig", "/Applications/XAMPP/xamppfiles/htdocs/weatherapp/templates/view/weather.html.twig");
    }
}
