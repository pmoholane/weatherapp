<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* view/weather.html.twig */
class __TwigTemplate_71f2a6212e2d73a69281fb96bb9f44dbe4e49b691f2c6b3faa27d2543ba85317 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "view/weather.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <title>Welcome to Symfony!</title>
        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
        <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    </head>
    <body>

        <div class=\"header py-2 px-5\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6\">
                    <img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo.jpeg"), "html", null, true);
        echo "\" class=\"logo\">
                </div>

                <div class=\"col-12 col-md-6\">
                    <div class=\"current_location pt-4 align-items-right\">
                    <strong><i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Current Location:</strong> <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 19, $this->source); })()), "location", [], "any", false, false, false, 19), "country_flag", [], "any", false, false, false, 19), "html", null, true);
        echo "\" width=\"15\" style=\"margin-top: -4px;\"> ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 19, $this->source); })()), "city", [], "any", false, false, false, 19), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 19, $this->source); })()), "region_name", [], "any", false, false, false, 19), "html", null, true);
        echo "  ";
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 19, $this->source); })()), "main", [], "any", false, false, false, 19), "temp", [], "any", false, false, false, 19) - 273.15), "html", null, true);
        echo "° C <img src=\"http://openweathermap.org/img/w/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 19, $this->source); })()), "weather", [], "any", false, false, false, 19), 0, [], "any", false, false, false, 19), "icon", [], "any", false, false, false, 19), "html", null, true);
        echo ".png\">
                    </div>
                </div>

            </div>

        </div>

        <div class=\"link-section\">
            <div class=\"container\">
                
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 pt-3\">
                        <ul class=\"links\">
                            <li><a href=\"\">TODAY</a></li>
                            <li><a href=\"\">HOURLY</a></li>
                            <li><a href=\"\">TOMOROW</a></li>
                            <li><a href=\"\">7 DAYS FORCAST</a></li>
                        </ul>
                    </div>

                    <div class=\"col-12 col-md-6\">
                        <form method=\"POST\" action=\"\" class=\"pt-4 align-items-right\">
                            <div class=\"form-row\">
                                ";
        // line 43
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 43, $this->source); })()), 'form_start');
        echo " 
                                ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 44, $this->source); })()), 'widget');
        echo " 
                                ";
        // line 45
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 45, $this->source); })()), 'form_end');
        echo "
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

        
            
        <div class=\"py-5\">
            <div class=\"container\">
                ";
        // line 58
        if (((isset($context["error_message"]) || array_key_exists("error_message", $context) ? $context["error_message"] : (function () { throw new RuntimeError('Variable "error_message" does not exist.', 58, $this->source); })()) != "")) {
            // line 59
            echo "                    <div class=\"alert alert-warning\" role=\"alert\">
                        ";
            // line 60
            echo twig_escape_filter($this->env, (isset($context["error_message"]) || array_key_exists("error_message", $context) ? $context["error_message"] : (function () { throw new RuntimeError('Variable "error_message" does not exist.', 60, $this->source); })()), "html", null, true);
            echo "
                    </div>
                ";
        }
        // line 63
        echo "
                ";
        // line 64
        if ((isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context))) {
            // line 65
            echo "                    <h3 class=\"mb-5\">Searched weather in ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["geolocation"]) || array_key_exists("geolocation", $context) ? $context["geolocation"] : (function () { throw new RuntimeError('Variable "geolocation" does not exist.', 65, $this->source); })()), "data", [], "any", false, false, false, 65), 0, [], "any", false, false, false, 65), "label", [], "any", false, false, false, 65), "html", null, true);
            echo ": ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 65, $this->source); })()), "weather", [], "any", false, false, false, 65), 0, [], "any", false, false, false, 65), "description", [], "any", false, false, false, 65), "html", null, true);
            echo " <img src=\"http://openweathermap.org/img/w/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 65, $this->source); })()), "weather", [], "any", false, false, false, 65), 0, [], "any", false, false, false, 65), "icon", [], "any", false, false, false, 65), "html", null, true);
            echo ".png\"></h3>
                ";
        } else {
            // line 67
            echo "                    <h3 class=\"mb-5\">Current Weather in ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 67, $this->source); })()), "city", [], "any", false, false, false, 67), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 67, $this->source); })()), "region_name", [], "any", false, false, false, 67), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 67, $this->source); })()), "country_name", [], "any", false, false, false, 67), "html", null, true);
            echo " is ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 67, $this->source); })()), "weather", [], "any", false, false, false, 67), 0, [], "any", false, false, false, 67), "description", [], "any", false, false, false, 67), "html", null, true);
            echo " <img src=\"http://openweathermap.org/img/w/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 67, $this->source); })()), "weather", [], "any", false, false, false, 67), 0, [], "any", false, false, false, 67), "icon", [], "any", false, false, false, 67), "html", null, true);
            echo ".png\"></h3>
                ";
        }
        // line 69
        echo "
                <div class=\"row\">

                    ";
        // line 72
        if ((isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context))) {
            // line 73
            echo "
                        <div class=\"col-12 col-md-6\">
                            <div class=\"weather-forcast-container p-5\">

                                ";
            // line 77
            if ((isset($context["geolocation"]) || array_key_exists("geolocation", $context) ? $context["geolocation"] : (function () { throw new RuntimeError('Variable "geolocation" does not exist.', 77, $this->source); })())) {
                // line 78
                echo "                                    <div class=\"conditions mb-4\">
                                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Location: <br/> 
                                        ";
                // line 80
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["geolocation"]) || array_key_exists("geolocation", $context) ? $context["geolocation"] : (function () { throw new RuntimeError('Variable "geolocation" does not exist.', 80, $this->source); })()), "data", [], "any", false, false, false, 80), 0, [], "any", false, false, false, 80), "label", [], "any", false, false, false, 80), "html", null, true);
                echo "
                                    </div>
                                ";
            } else {
                // line 83
                echo "                                    <div class=\"conditions mb-4\">
                                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Location: <br/> 
                                        ";
                // line 85
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 85, $this->source); })()), "city", [], "any", false, false, false, 85), "html", null, true);
                echo ", ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 85, $this->source); })()), "region_name", [], "any", false, false, false, 85), "html", null, true);
                echo ", ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 85, $this->source); })()), "country_name", [], "any", false, false, false, 85), "html", null, true);
                echo " <img src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 85, $this->source); })()), "location", [], "any", false, false, false, 85), "country_flag", [], "any", false, false, false, 85), "html", null, true);
                echo "\" width=\"17\">
                                    </div>
                                ";
            }
            // line 88
            echo "                                

                                <div class=\"row\">
                                    
                                    <div class=\"col-6 conditions\">
                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-thermometer-empty\" aria-hidden=\"true\"></i> Temperature: <br/>
                                            <div class=\"temp mt-2\">Avg: ";
            // line 95
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 95, $this->source); })()), "main", [], "any", false, false, false, 95), "temp", [], "any", false, false, false, 95) - 273.15), "html", null, true);
            echo "°C</div>
                                            <div class=\"temp\">Feels Like: ";
            // line 96
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 96, $this->source); })()), "main", [], "any", false, false, false, 96), "feels_like", [], "any", false, false, false, 96) - 273.15), "html", null, true);
            echo "°C</div>
                                            <div class=\"temp\">Min: ";
            // line 97
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 97, $this->source); })()), "main", [], "any", false, false, false, 97), "temp_min", [], "any", false, false, false, 97) - 273.15), "html", null, true);
            echo "°C</div>
                                            <div class=\"temp\">Max: ";
            // line 98
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 98, $this->source); })()), "main", [], "any", false, false, false, 98), "temp_max", [], "any", false, false, false, 98) - 273.15), "html", null, true);
            echo "°C</div>
                                            <div class=\"temp\">Pressure: ";
            // line 99
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 99, $this->source); })()), "main", [], "any", false, false, false, 99), "pressure", [], "any", false, false, false, 99), "html", null, true);
            echo " hPa</div>
                                            <div class=\"temp\">Humidity: ";
            // line 100
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 100, $this->source); })()), "main", [], "any", false, false, false, 100), "humidity", [], "any", false, false, false, 100), "html", null, true);
            echo "% </div>
                                        </div>
                                    </div>

                                    <div class=\"col-6 conditions\">

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Wind Direction:
                                            <span>";
            // line 108
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 108, $this->source); })()), "wind", [], "any", false, false, false, 108), "deg", [], "any", false, false, false, 108), "html", null, true);
            echo "°</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-tint\" aria-hidden=\"true\"></i> Precipitation:
                                            <span>";
            // line 113
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 113, $this->source); })()), "main", [], "any", false, false, false, 113), "temp", [], "any", false, false, false, 113) - 273.15), "html", null, true);
            echo "</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-flag\" aria-hidden=\"true\"></i> Wind Speed:
                                            <span>";
            // line 118
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 118, $this->source); })()), "wind", [], "any", false, false, false, 118), "speed", [], "any", false, false, false, 118), "html", null, true);
            echo " meter/sec</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Cloudiness:
                                            <span>";
            // line 123
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["newWeatherLocation"]) || array_key_exists("newWeatherLocation", $context) ? $context["newWeatherLocation"] : (function () { throw new RuntimeError('Variable "newWeatherLocation" does not exist.', 123, $this->source); })()), "clouds", [], "any", false, false, false, 123), "all", [], "any", false, false, false, 123), "html", null, true);
            echo "%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    ";
        } else {
            // line 132
            echo "                        <div class=\"col-12 col-md-6\">
                            <div class=\"weather-forcast-container p-5\">

                                ";
            // line 135
            if ((isset($context["geolocation"]) || array_key_exists("geolocation", $context))) {
                // line 136
                echo "                                    <div class=\"conditions mb-4\">
                                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Location: <br/> 
                                        ";
                // line 138
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["geolocation"]) || array_key_exists("geolocation", $context) ? $context["geolocation"] : (function () { throw new RuntimeError('Variable "geolocation" does not exist.', 138, $this->source); })()), "data", [], "any", false, false, false, 138), 0, [], "any", false, false, false, 138), "label", [], "any", false, false, false, 138), "html", null, true);
                echo "
                                    </div>
                                ";
            } else {
                // line 141
                echo "                                    <div class=\"conditions mb-4\">
                                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Location: <br/> 
                                        ";
                // line 143
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 143, $this->source); })()), "city", [], "any", false, false, false, 143), "html", null, true);
                echo ", ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 143, $this->source); })()), "region_name", [], "any", false, false, false, 143), "html", null, true);
                echo ", ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 143, $this->source); })()), "region_name", [], "any", false, false, false, 143), "html", null, true);
                echo ", ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 143, $this->source); })()), "country_name", [], "any", false, false, false, 143), "html", null, true);
                echo " <img src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ip_lookup"]) || array_key_exists("ip_lookup", $context) ? $context["ip_lookup"] : (function () { throw new RuntimeError('Variable "ip_lookup" does not exist.', 143, $this->source); })()), "location", [], "any", false, false, false, 143), "country_flag", [], "any", false, false, false, 143), "html", null, true);
                echo "\" width=\"17\">
                                    </div>
                                ";
            }
            // line 146
            echo "                                

                                <div class=\"row\">
                                    
                                    <div class=\"col-6 conditions\">
                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-thermometer-empty\" aria-hidden=\"true\"></i> Temperature: <br/>
                                            <div class=\"temp mt-2\">Avg: ";
            // line 153
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 153, $this->source); })()), "main", [], "any", false, false, false, 153), "temp", [], "any", false, false, false, 153) - 273.15), "html", null, true);
            echo "°C</div>
                                            <div class=\"temp\">Feels Like: ";
            // line 154
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 154, $this->source); })()), "main", [], "any", false, false, false, 154), "feels_like", [], "any", false, false, false, 154) - 273.15), "html", null, true);
            echo "°C</div>
                                            <div class=\"temp\">Min: ";
            // line 155
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 155, $this->source); })()), "main", [], "any", false, false, false, 155), "temp_min", [], "any", false, false, false, 155) - 273.15), "html", null, true);
            echo "°C</div>
                                            <div class=\"temp\">Max: ";
            // line 156
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 156, $this->source); })()), "main", [], "any", false, false, false, 156), "temp_max", [], "any", false, false, false, 156) - 273.15), "html", null, true);
            echo "°C</div>
                                            <div class=\"temp\">Pressure: ";
            // line 157
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 157, $this->source); })()), "main", [], "any", false, false, false, 157), "pressure", [], "any", false, false, false, 157), "html", null, true);
            echo " hPa</div>
                                            <div class=\"temp\">Humidity: ";
            // line 158
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 158, $this->source); })()), "main", [], "any", false, false, false, 158), "humidity", [], "any", false, false, false, 158), "html", null, true);
            echo "% </div>
                                        </div>
                                    </div>

                                    <div class=\"col-6 conditions\">

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Wind Direction:
                                            <span>";
            // line 166
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 166, $this->source); })()), "wind", [], "any", false, false, false, 166), "deg", [], "any", false, false, false, 166), "html", null, true);
            echo "°</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-tint\" aria-hidden=\"true\"></i> Precipitation:
                                            <span>";
            // line 171
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 171, $this->source); })()), "main", [], "any", false, false, false, 171), "temp", [], "any", false, false, false, 171) - 273.15), "html", null, true);
            echo "</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-flag\" aria-hidden=\"true\"></i> Wind Speed:
                                            <span>";
            // line 176
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 176, $this->source); })()), "wind", [], "any", false, false, false, 176), "speed", [], "any", false, false, false, 176), "html", null, true);
            echo " meter/sec</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Cloudiness:
                                            <span>";
            // line 181
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["currentWeather"]) || array_key_exists("currentWeather", $context) ? $context["currentWeather"] : (function () { throw new RuntimeError('Variable "currentWeather" does not exist.', 181, $this->source); })()), "clouds", [], "any", false, false, false, 181), "all", [], "any", false, false, false, 181), "html", null, true);
            echo "%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    ";
        }
        // line 189
        echo "

                    <div class=\"col-12 col-md-6 sa-weather-service\">
                        <h4>South African Weather Service: Weather Alerts</h4>
                        <div class=\"py-3\">
                            <h6>Mossel Bay - Damaging Winds</h6>
                            <div class=\"time-period mb-2\"><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i> 2023/09/15 00:00 - 2023/09/17 23:59:59</div>
                            <p>
                                Difficulty in navigation at sea for small vessels and personal water crafts (e.g. kayaks). 
                                Localised disruptions at susceptible ports due to quick, successive steep waves. 
                                Small vessels at risk of mooring lines breaking and taking on water and capsizing in locality. 
                                Localised build-up of sand on coastal routs. Localised disruption to beachfront activities.
                            </p>

                            <hr>

                            <div class=\"py-3\">
                                <h6>Lamberts Bay - Damaging Winds</h6>
                                <div class=\"time-period mb-2\"><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i> 2023/09/15 00:00 - 2023/09/17 23:59:59</div>
                                <p>
                                    Difficulty in navigation at sea for small vessels and personal water crafts (e.g. kayaks). 
                                    Localised disruptions at susceptible ports due to quick, successive steep waves. 
                                    Small vessels at risk of mooring lines breaking and taking on water and capsizing in locality. 
                                    Localised build-up of sand on coastal routs. Localised disruption to beachfront activities.
                                </p>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

            
            
        <footer class=\"footer py-3\">
            <div class=\"container text-center\">
                <span class=\"text-muted\">Developed by Phaladi Moholane for RSAWeb Assessment</span>
            </div>
        </footer>
       
        <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>
    </body>
</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "view/weather.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  392 => 189,  381 => 181,  373 => 176,  365 => 171,  357 => 166,  346 => 158,  342 => 157,  338 => 156,  334 => 155,  330 => 154,  326 => 153,  317 => 146,  303 => 143,  299 => 141,  293 => 138,  289 => 136,  287 => 135,  282 => 132,  270 => 123,  262 => 118,  254 => 113,  246 => 108,  235 => 100,  231 => 99,  227 => 98,  223 => 97,  219 => 96,  215 => 95,  206 => 88,  194 => 85,  190 => 83,  184 => 80,  180 => 78,  178 => 77,  172 => 73,  170 => 72,  165 => 69,  151 => 67,  141 => 65,  139 => 64,  136 => 63,  130 => 60,  127 => 59,  125 => 58,  109 => 45,  105 => 44,  101 => 43,  66 => 19,  58 => 14,  48 => 7,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <title>Welcome to Symfony!</title>
        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
        <link href=\"{{ asset('css/custom.css') }}\" rel=\"stylesheet\" />
    </head>
    <body>

        <div class=\"header py-2 px-5\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6\">
                    <img src=\"{{ asset('images/logo.jpeg') }}\" class=\"logo\">
                </div>

                <div class=\"col-12 col-md-6\">
                    <div class=\"current_location pt-4 align-items-right\">
                    <strong><i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Current Location:</strong> <img src=\"{{ ip_lookup.location.country_flag }}\" width=\"15\" style=\"margin-top: -4px;\"> {{ ip_lookup.city }}, {{ ip_lookup.region_name }}  {{currentWeather.main.temp - 273.15}}° C <img src=\"http://openweathermap.org/img/w/{{currentWeather.weather.0.icon}}.png\">
                    </div>
                </div>

            </div>

        </div>

        <div class=\"link-section\">
            <div class=\"container\">
                
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 pt-3\">
                        <ul class=\"links\">
                            <li><a href=\"\">TODAY</a></li>
                            <li><a href=\"\">HOURLY</a></li>
                            <li><a href=\"\">TOMOROW</a></li>
                            <li><a href=\"\">7 DAYS FORCAST</a></li>
                        </ul>
                    </div>

                    <div class=\"col-12 col-md-6\">
                        <form method=\"POST\" action=\"\" class=\"pt-4 align-items-right\">
                            <div class=\"form-row\">
                                {{ form_start(form) }} 
                                {{ form_widget(form) }} 
                                {{ form_end(form) }}
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

        
            
        <div class=\"py-5\">
            <div class=\"container\">
                {% if error_message != '' %}
                    <div class=\"alert alert-warning\" role=\"alert\">
                        {{error_message}}
                    </div>
                {% endif %}

                {% if newWeatherLocation is defined %}
                    <h3 class=\"mb-5\">Searched weather in {{geolocation.data.0.label}}: {{newWeatherLocation.weather.0.description}} <img src=\"http://openweathermap.org/img/w/{{newWeatherLocation.weather.0.icon}}.png\"></h3>
                {% else %}
                    <h3 class=\"mb-5\">Current Weather in {{ip_lookup.city}}, {{ip_lookup.region_name}}, {{ip_lookup.country_name}} is {{currentWeather.weather.0.description}} <img src=\"http://openweathermap.org/img/w/{{currentWeather.weather.0.icon}}.png\"></h3>
                {% endif %}

                <div class=\"row\">

                    {% if newWeatherLocation is defined %}

                        <div class=\"col-12 col-md-6\">
                            <div class=\"weather-forcast-container p-5\">

                                {% if geolocation %}
                                    <div class=\"conditions mb-4\">
                                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Location: <br/> 
                                        {{geolocation.data.0.label}}
                                    </div>
                                {% else %}
                                    <div class=\"conditions mb-4\">
                                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Location: <br/> 
                                        {{ip_lookup.city}}, {{ip_lookup.region_name}}, {{ip_lookup.country_name}} <img src=\"{{ip_lookup.location.country_flag}}\" width=\"17\">
                                    </div>
                                {% endif %}
                                

                                <div class=\"row\">
                                    
                                    <div class=\"col-6 conditions\">
                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-thermometer-empty\" aria-hidden=\"true\"></i> Temperature: <br/>
                                            <div class=\"temp mt-2\">Avg: {{newWeatherLocation.main.temp - 273.15}}°C</div>
                                            <div class=\"temp\">Feels Like: {{newWeatherLocation.main.feels_like - 273.15}}°C</div>
                                            <div class=\"temp\">Min: {{newWeatherLocation.main.temp_min - 273.15}}°C</div>
                                            <div class=\"temp\">Max: {{newWeatherLocation.main.temp_max - 273.15}}°C</div>
                                            <div class=\"temp\">Pressure: {{newWeatherLocation.main.pressure}} hPa</div>
                                            <div class=\"temp\">Humidity: {{newWeatherLocation.main.humidity}}% </div>
                                        </div>
                                    </div>

                                    <div class=\"col-6 conditions\">

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Wind Direction:
                                            <span>{{newWeatherLocation.wind.deg}}°</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-tint\" aria-hidden=\"true\"></i> Precipitation:
                                            <span>{{newWeatherLocation.main.temp - 273.15}}</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-flag\" aria-hidden=\"true\"></i> Wind Speed:
                                            <span>{{newWeatherLocation.wind.speed}} meter/sec</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Cloudiness:
                                            <span>{{newWeatherLocation.clouds.all}}%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    {% else %}
                        <div class=\"col-12 col-md-6\">
                            <div class=\"weather-forcast-container p-5\">

                                {% if geolocation is defined %}
                                    <div class=\"conditions mb-4\">
                                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Location: <br/> 
                                        {{geolocation.data.0.label}}
                                    </div>
                                {% else %}
                                    <div class=\"conditions mb-4\">
                                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i> Location: <br/> 
                                        {{ip_lookup.city}}, {{ip_lookup.region_name}}, {{ip_lookup.region_name}}, {{ip_lookup.country_name}} <img src=\"{{ip_lookup.location.country_flag}}\" width=\"17\">
                                    </div>
                                {% endif %}
                                

                                <div class=\"row\">
                                    
                                    <div class=\"col-6 conditions\">
                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-thermometer-empty\" aria-hidden=\"true\"></i> Temperature: <br/>
                                            <div class=\"temp mt-2\">Avg: {{currentWeather.main.temp - 273.15}}°C</div>
                                            <div class=\"temp\">Feels Like: {{currentWeather.main.feels_like - 273.15}}°C</div>
                                            <div class=\"temp\">Min: {{currentWeather.main.temp_min - 273.15}}°C</div>
                                            <div class=\"temp\">Max: {{currentWeather.main.temp_max - 273.15}}°C</div>
                                            <div class=\"temp\">Pressure: {{currentWeather.main.pressure}} hPa</div>
                                            <div class=\"temp\">Humidity: {{currentWeather.main.humidity}}% </div>
                                        </div>
                                    </div>

                                    <div class=\"col-6 conditions\">

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Wind Direction:
                                            <span>{{currentWeather.wind.deg}}°</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-tint\" aria-hidden=\"true\"></i> Precipitation:
                                            <span>{{currentWeather.main.temp - 273.15}}</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-flag\" aria-hidden=\"true\"></i> Wind Speed:
                                            <span>{{currentWeather.wind.speed}} meter/sec</span>
                                        </div>

                                        <div class=\"pb-3\">
                                            <i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> Cloudiness:
                                            <span>{{currentWeather.clouds.all}}%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    {% endif %}


                    <div class=\"col-12 col-md-6 sa-weather-service\">
                        <h4>South African Weather Service: Weather Alerts</h4>
                        <div class=\"py-3\">
                            <h6>Mossel Bay - Damaging Winds</h6>
                            <div class=\"time-period mb-2\"><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i> 2023/09/15 00:00 - 2023/09/17 23:59:59</div>
                            <p>
                                Difficulty in navigation at sea for small vessels and personal water crafts (e.g. kayaks). 
                                Localised disruptions at susceptible ports due to quick, successive steep waves. 
                                Small vessels at risk of mooring lines breaking and taking on water and capsizing in locality. 
                                Localised build-up of sand on coastal routs. Localised disruption to beachfront activities.
                            </p>

                            <hr>

                            <div class=\"py-3\">
                                <h6>Lamberts Bay - Damaging Winds</h6>
                                <div class=\"time-period mb-2\"><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i> 2023/09/15 00:00 - 2023/09/17 23:59:59</div>
                                <p>
                                    Difficulty in navigation at sea for small vessels and personal water crafts (e.g. kayaks). 
                                    Localised disruptions at susceptible ports due to quick, successive steep waves. 
                                    Small vessels at risk of mooring lines breaking and taking on water and capsizing in locality. 
                                    Localised build-up of sand on coastal routs. Localised disruption to beachfront activities.
                                </p>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

            
            
        <footer class=\"footer py-3\">
            <div class=\"container text-center\">
                <span class=\"text-muted\">Developed by Phaladi Moholane for RSAWeb Assessment</span>
            </div>
        </footer>
       
        <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>
    </body>
</html>", "view/weather.html.twig", "/Applications/XAMPP/xamppfiles/htdocs/weatherapp/templates/view/weather.html.twig");
    }
}
